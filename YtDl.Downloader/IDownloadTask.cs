﻿using System.ComponentModel;
using System.Threading.Tasks;

namespace YtDl.Downloader
{
    public interface IDownloadTask : INotifyPropertyChanged
    {
        Task<DownloadResult> Start();
        void Cancel();

        int ProgressPercent { get; }
        string Name { get; }
    }
}