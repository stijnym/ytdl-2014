﻿namespace YtDl.Downloader
{
    public interface IServices
    {
        IDownloadTask CreateDownloadTask(string name, string url, string target);
    }
}