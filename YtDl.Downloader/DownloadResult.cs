﻿namespace YtDl.Downloader
{
    public class DownloadResult
    {
        public DownloadResult(bool cancelled, int progressPercent)
        {
            ProgressPercent = progressPercent;
            Cancelled = cancelled;
        }

        public bool Cancelled { get; private set; }

        public int ProgressPercent { get; private set; }
    }
}