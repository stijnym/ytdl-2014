﻿using System.Collections.ObjectModel;

namespace Ui.Wpf
{
    /// <summary>
    /// Interaction logic for DownloadTasks.xaml
    /// </summary>
    public partial class DownloadTasks
    {
        public ObservableCollection<DownloadTaskUi> DownloadTaskObjects { get; private set; }

        public DownloadTasks()
        {
            DownloadTaskObjects = new ObservableCollection<DownloadTaskUi>();
            InitializeComponent();
        }

        public void Add(DownloadTaskUi taskUi)
        {
            DownloadTaskObjects.Add(taskUi);
        }

        public void Remove(DownloadTaskUi taskUi)
        {
            DownloadTaskObjects.Remove(taskUi);
        }
    }
}
