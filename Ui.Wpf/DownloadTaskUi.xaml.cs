﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls.Primitives;
using YtDl.Downloader;

namespace Ui.Wpf
{
    /// <summary>
    /// Interaction logic for DownloadTaskUi.xaml
    /// </summary>
    public partial class DownloadTaskUi : INotifyPropertyChanged
    {
        private string _statusText;

        public DownloadTaskUi()
        {
            InitializeComponent();
        }

        public DownloadTaskUi(IDownloadTask task)
        {
            Task = task;
            StatusText = "Idle";

            InitializeComponent();
        }

        public IDownloadTask Task { get; private set; }

        public string StatusText
        {
            get { return _statusText; }
            private set { _statusText = value; OnPropertyChanged(); }
        }

        async void Start()
        {
            StatusText = "Downloading";
            var result = await Task.Start();

            // Todo: Set to appropriate state
            StatusText = "Done";
        }

        void Cancel()
        {
            Task.Cancel();
        }

        private void ButtonStart_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonStart.IsEnabled = false;
            Start();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
