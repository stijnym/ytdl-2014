﻿using System.Windows;

namespace Ui.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        internal static MainWindow Singleton;

        public MainWindow()
        {
            InitializeComponent();
            Singleton = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DownloadTasks.Add(new DownloadTaskUi(Global.Services.CreateDownloadTask("Test Name", tmpText.Text, tmpText2.Text)));
        }
    }
}
