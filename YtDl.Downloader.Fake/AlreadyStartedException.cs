﻿using System;
using System.Runtime.Serialization;

namespace YtDl.Downloader.Fake
{
    [Serializable]
    public class AlreadyStartedException : Exception
    {
        public AlreadyStartedException()
        {
        }

        public AlreadyStartedException(string message) : base(message)
        {
        }

        public AlreadyStartedException(string message, Exception inner) : base(message, inner)
        {
        }

        protected AlreadyStartedException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}