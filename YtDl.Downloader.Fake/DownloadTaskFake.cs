﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace YtDl.Downloader.Fake
{
    public class DownloadTaskFake : IDownloadTask
    {
        private CancellationTokenSource _cancelToken;
        private int _progressPercent;

        public DownloadTaskFake(string name, string url, string target)
        {
            Name = name;
            Target = target;
            Url = url;
        }

        #region Properties

        public string Name { get; private set; }
        public string Url { get; private set; }
        public string Target { get; private set; }

        // State
        public int ProgressPercent
        {
            get { return _progressPercent; }
            private set { _progressPercent = value; OnPropertyChanged(); }
        }

        #endregion

        public async Task<DownloadResult> Start()
        {
            if (_cancelToken != null)
                throw new AlreadyStartedException("Already started");

            _cancelToken = new CancellationTokenSource();
            
            while (ProgressPercent < 100)
            {
                if (_cancelToken.IsCancellationRequested)
                    break;
                ProgressPercent += 1;
                await Task.Delay(100);
            }

            return new DownloadResult(_cancelToken.IsCancellationRequested, ProgressPercent);
        }

        public void Cancel()
        {
            _cancelToken.Cancel();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}