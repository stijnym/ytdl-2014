﻿using System;
using System.Collections.Generic;
using Ui.Wpf;
using YtDl.Downloader;

namespace Launcher
{
    public class Launch
    {
        [STAThread]
        public static void Main()
        {
            Global.Services = new Services();

            var mainWindow = new MainWindow();
            new App { MainWindow = mainWindow }.MainWindow.ShowDialog();
        }
    }
}