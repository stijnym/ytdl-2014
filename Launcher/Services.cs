﻿using Ui.Wpf;
using YtDl.Downloader;
using YtDl.Downloader.Fake;

namespace Launcher
{
    class Services : IServices
    {
        public IDownloadTask CreateDownloadTask(string name, string url, string target)
        {
            return new DownloadTaskFake(name, url, target);
        }
    }
}